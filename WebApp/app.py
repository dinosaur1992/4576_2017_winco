import sys
import os
import argparse
from dateutil.parser import parse
from shutil import copyfile
from flask import Flask, render_template, request, jsonify

sys.path.append(os.path.abspath('..\\Scripts'))
try:
    import rack_frequency_analysis
    import data_processor
    from DistanceAnalyzer import DistanceAnalyzer
except:
    pass

# Global variable
heat_map_image_src = '..\\Data\\heat_map.png'
heat_map_image_dest = '.\\static\\image\\heat_map.png'
data = None

app = Flask(__name__)


@app.route('/')
def homepage():
    return render_template("home.html", dic={})


@app.route('/_load_order')
def load_order():
    start_date = parse(request.args.get('start_date')).date()
    end_date = parse(request.args.get('end_date')).date()

    order_list = data["order_list"].order_list.values()
    filtered_order_list = [order for order in order_list if start_date <= order.order_date <= end_date]
    order_json = []
    for order in filtered_order_list:
        if order.required_date:
            order_json.append({"order_id": str(order.id),
                               "order_date": order.order_date.strftime('%m/%d/%Y'),
                               "required_date": order.required_date.strftime('%m/%d/%Y')
                               })
        else:
            order_json.append({"order_id": str(order.id),
                               "order_date": order.order_date.strftime('%m/%d/%Y'),
                               "required_date": ""
                               })
    return jsonify(order_json)


@app.route('/_draw_heat_map')
def draw_heat_map():
    start_date = request.args.get('start_date', 0, type=str)
    end_date = request.args.get('end_date', 0, type=str)
    rack_frequency_analysis.main(data=data, date_range=(start_date, end_date))
    copyfile(heat_map_image_src, heat_map_image_dest)
    return jsonify(result=True)


@app.route('/order/<order_id>', methods=['GET', 'POST'])
def shortest_path_per_order(order_id):
    order_id = int(order_id)
    da = DistanceAnalyzer(data)
    shortest_distance, distance_dict, item_path_dict = da.analyze_distance(orders=[order_id], use_new_loc=True)

    path = []
    item_path = []
    for value in item_path_dict.values():
        item_path.extend(value)

    for item_id in item_path:
        order_list = data["order_list"].order_list
        order_item = order_list[order_id].get_item(item_id)
        if order_item:
            order_qty = order_item.ord_qty
            location = data["new_item_list"][item_id].location
            picking_zone = data["location_list"][location].picking_zone.id
            path.append({"picking_zone": picking_zone, "rack_location": location, "item_id": item_id, "quantity": order_qty})

    return render_template("order_shortest_path.html", order_id=order_id, shortest_path=path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--debug', action='store_true')
    args = parser.parse_args()

    if not data:
        data = data_processor.main(debug=args.debug)
    app.debug = args.debug
    app.run()
