
initiate_datepicker = function() {
	$( function() {
	    $.each($( ".datepicker" ), function(index, value){ $(value).datepicker() });
	  } );
}

draw_heat_map = function() {
	$('#update_view').click(function() {
		$.getJSON('/_draw_heat_map', {
			start_date: $('input[name="map_start_date"]').val(),
			end_date: $('input[name="map_end_date"]').val()
		}, function(data) {
		    if(data) {
		        src = $("#heat_map_image").attr("src")
		        src = src.split("?")[0]
		        rand = Date.now()
		        $("#heat_map_image").attr("src", src + '?random=' + rand);
		    }
		})
		return false;
	})
}

filter_order = function() {
	$('#load_order').click(function() {
		$.getJSON('/_load_order', {
			start_date: $('input[name="order_start_date"]').val(),
			end_date: $('input[name="order_end_date"]').val()
		}, function(data) {
		    if(data) {
		        table = $($('.datatable')[0]).dataTable();
		        table.fnClearTable();
		        $.each(data, function() {
                    table.fnAddData([
                        '<a href="/order/' + this.order_id + '", target="_blank">' + this.order_id + '</a>',
                        this.order_date,
                        this.required_date
                    ] );
                });
		    }
		})
		return false;
	})
}

initiate_data_table = function() {
	$( function() {
	    $.each($(".datatable"), function(index, value){ $(value).DataTable() });
	});
	$( function() {
	    $.each($(".simple_datatable"),
	            function(index, value){
	                $(value).DataTable({
	                    'bSort': false,
	                    'searching': false,
	                    'iDisplayLength': 50
	                })
	            })
	})
}

manage_section = function() {
    $('a[href="#heat_map"]').click(function() {
        $.each($('.w3-section'),
                function(index, value) {
                    $(value).hide()
                });
        $('#heat_map').show();
    });

    $('a[href="#shortest_path"]').click(function() {
        $.each($('.w3-section'),
                function(index, value) {
                    $(value).hide()
                });
        $('#shortest_path').show();
    });

    $.each($('.w3-section'),
            function(index, value) {
                $(value).hide()
            });
    if (location.hash) {
        $(location.hash).show();
    }
    else {
        $($('.w3-section')[0]).show()
    }

}

window.onload = function () {
    manage_section()
	initiate_datepicker()
	draw_heat_map()
	initiate_data_table()
	filter_order()
}
