
import pandas
import xlwings as xw
import data_processor
import pythoncom

from dateutil.parser import parse

rack_frequency_sheet_name = "rack_frequency"
file_path = '..\\Data\\Heat_Map_Generator.xlsm'
macro_name = "SaveJPG"


# Output rack frequency to Excel and update heat map image by calling Macro
def generate_output(rack_frequency):
    pythoncom.CoInitialize()
    book = xw.Book(file_path)
    book.DisplayAlerts = False
    sheet = book.sheets[rack_frequency_sheet_name]
    sheet.clear_contents()

    df = pandas.DataFrame.from_dict(rack_frequency, orient="index")
    sheet.range('A1').value = df
    book.save()

    macro = book.app.macro(macro_name)
    macro()
    book.app.quit()

    app = xw.App()
    app.DisplayAlerts = True
    app.quit()


# Calculate rack frequency based on order information within the given date range
# distance_analyzer is a DistanceAnalyzer object.
# date_range is a tuple in the format of (start_date, end_date) in date strings
def generate_rack_frequency(data=None, order_list=None, item_list=None, location_list=None, date_range=None):
    if not data:
        data = data_processor.main()
    if not order_list:
        order_list = data["order_list"].order_list.values()
    if not item_list:
        item_list = data['new_item_list']
    if not location_list:
        location_list = data['location_list']

    if date_range:
        start_date = parse(date_range[0]).date()
        end_date = parse(date_range[1]).date()
        order_list = [order for order in order_list if start_date <= order.order_date <= end_date]

    # Generate empty dictionary
    rack_frequency = {}
    for location in location_list.keys():
        rack_frequency[location] = 0

    # Loop over order to count rack frequency
    for order in order_list:
        items = order.items
        order_location = []
        for item in items:
            item_no = item.item_no
            if item_no in item_list.keys():
                location_name = item_list[item_no].location
                if location_name not in order_location:
                    order_location.append(location_name)
                    if location_name in rack_frequency.keys():
                        rack_frequency[location_name] += 1
            else:
                print("Failed to find item ID, ", item_no)

    return rack_frequency


# Main function
def main(data=None, order_list=None, item_list=None, location_list=None, date_range=None):
    rack_frequency = generate_rack_frequency(data, order_list, item_list, location_list, date_range)
    generate_output(rack_frequency)

if __name__ == "__main__":
    main()
