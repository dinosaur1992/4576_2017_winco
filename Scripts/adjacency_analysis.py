import data_processor

adjacency_file = '..\\Data\\Adjacency.csv'


# Calculate item order frequency
def analyze_frequency(item_list, order_list):
    # Construct array
    frequency = {}
    for item_no in item_list:
        frequency[item_no] = 0

    for order in order_list.order_list.values():
        items = order.items
        for item in items:
            item_no = item.item_no
            frequency[item_no] += 1
    return frequency


# Calculate affinity order frequency for each pair of items
def analyze_adjacency(item_list, order_list):
    # Construct Matrix
    adjacency_matrix = {}
    for item_no in item_list:
        adjacency_matrix[item_no] = {}
        for adjacent_item_no in item_list:
            adjacency_matrix[item_no][adjacent_item_no] = 0

    # Loop through orders
    for order in order_list.order_list.values():
        items = order.items
        for pivot_item in items:
            pivot_item_no = pivot_item.item_no
            for item in items:
                item_no = item.item_no
                if item_no != pivot_item_no:
                    adjacency_matrix[pivot_item_no][item_no] += 1
    return adjacency_matrix


# Output affinity data into a csv file
def output_adjacency(adjacency_matrix):
    item_nos = adjacency_matrix.keys()
    file = open(adjacency_file, 'w')
    # Write notes
    file.write('Note: \n')
    file.write(',1. Diagonal cells are always zero. \n')
    file.write(',2. Other cells denote how many order contains both items. \n')
    # Write header
    file.write(',' + ','.join(str(h) for h in item_nos) + '\n')
    # Write each line
    for item_no in item_nos:
        file.write(str(item_no) + ',' + ','.join(str(v) for v in adjacency_matrix[item_no].values()) + '\n')
    file.close()


# Main function
def main():
    data = data_processor.main()
    order_list = data["order_list"]
    item_list = data["item_list"].keys()
    print('Analyzing adjacency...')
    adjacency_matrix = analyze_adjacency(item_list, order_list)
    print('Generating adjacency matrix...')
    output_adjacency(adjacency_matrix)
    print('Adjacency analysis finished, ')


if __name__ == "__main__":
    main()
