import random
import math


# Helper class to run simulated annealing
class Node:
    def __init__(self, matrix, path):
        self.path = path
        self.cost = self.calculate_cost(matrix)
        self.neighbors = None

    # calculate cost of current path
    def calculate_cost(self, matrix):
        path = self.path
        distance = 0
        if len(path) > 0:
            distance += matrix[0][path[0]]
            for i in range(len(path) - 1):
                distance += matrix[path[i]][path[i+1]]
            distance += matrix[path[-1]][0]
        return distance

    # Set up neigh of current node
    @property
    def neigh(self):
        if self.neighbors:
            return self.neighbors
        else:
            self.neighbors = self.generate_neighbors()
            return self.neighbors

    # Helper function to find all possible neighbors of current node.
    # Neighbor is defined as: given current path, all paths that swap any of 2 elements from the original path
    def generate_neighbors(self):
        neighbors = []
        path = self.path
        for i in range(1, len(path)):
            for j in range(i + 1, len(path) + 1):
                new_path = []
                new_path.extend(path[:i - 1])
                new_path.append(path[j - 1])
                new_path.extend(path[i: j - 1])
                new_path.append(path[i - 1])
                new_path.extend(path[j:])
                neighbors.append(new_path)
        return neighbors


def initial_temp(matrix):
    # Find a relative most expensive route by picking up an item and come back.
    return sum(matrix[0]) * 2


# Increase the temperature to make this neighbor a very likely choice
# This function may not decrease the temperature
def dependant_initial_t(temperature, node, neighbor):
    x = temperature / (node.cost - neighbor.cost + 0.5) * math.log1p(100 / 99)
    return temperature / x if 1 > x > 0 else temperature


# common function for temperature reduction
def reduce_t(temperature, repetitions):
    return temperature / math.log(repetitions + 2, 2)


# Main simulated annealing function to maintain multi threading environment and find the best path eventually
def sa(matrix):
    start_path = list(range(1, len(matrix)))
    start_node = Node(matrix, start_path)
    node = start_node

    if len(matrix) > 1:
        d = len(start_node.neigh)  # number of repetitions per temperature value
        temperature = initial_temp(matrix)

        repetitions = 0
        while temperature > 0.001 and node.cost > len(matrix) and d > 0:
            neighbor = Node(matrix, random.choice(node.neigh))  # choose random neigh
            if neighbor.cost < node.cost:
                node = neighbor
            else:
                annealing = random.uniform(0, 1)
                if annealing < math.exp(-(neighbor.cost - node.cost) / temperature):
                    node = neighbor
            repetitions += 1

            if repetitions < d:
                # set a dependant initial temperature
                temperature = dependant_initial_t(temperature, node, neighbor)
            elif repetitions % d == 0:
                temperature = reduce_t(temperature, repetitions)
        # end while

    final_path = [0]
    if len(node.path) > 0:
        final_path.extend(node.path)
        final_path.extend([0])
    # print("final_path", final_path)
    return {"distance": node.cost, "visited": final_path}


# Local test method
def test():
    test_matrix = [[0,1,2,3,4,5,6,1,8,7],
                   [1,0,1,2,6,4,5,2,6,1],
                   [2,1,0,3,4,1,2,3,8,3],
                   [3,2,3,0,3,6,9,8,2,7],
                   [4,6,4,3,0,5,2,8,7,5],
                   [5,4,1,6,5,0,2,2,7,3],
                   [6,5,2,9,2,2,0,7,3,9],
                   [1,2,3,8,8,2,7,0,4,2],
                   [8,6,8,2,7,7,3,4,0,1],
                   [7,1,3,7,5,3,9,2,1,0]]
    # test_matrix = [[0]]
    # print(initial_temp(test_matrix))
    import datetime
    from collections import Counter
    results = []
    start = datetime.datetime.now()
    # print(sa(test_matrix))
    for i in range(1000):
        print(i)
        results.append(sa(test_matrix)["distance"])
    end = datetime.datetime.now()
    print(Counter(results))
    print(end - start)

if __name__ == "__main__":
    test()
