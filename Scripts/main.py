import data_processor
import optimal_layout

from DistanceAnalyzer import DistanceAnalyzer


def main():
    data = data_processor.main()
    da = DistanceAnalyzer(data)
    da.calculate_avg_distance()
    result_da = optimal_layout.main(data)
    result_da.calculate_avg_distance()

if __name__ == "__main__":
    main()
