from Order import Order


class OrderList:

    def __init__(self):
        self.order_list = {}

    def add_order(self, order):
        if type(order) is Order:
            if not self.contain_order(order.id):
                self.order_list[order.id] = order

    def contain_order(self, order_id):
        if order_id in self.order_list.keys():
            return True
        else:
            return False

    def get_order(self, order_id):
        if self.contain_order(order_id):
            return self.order_list[order_id]
        return None
