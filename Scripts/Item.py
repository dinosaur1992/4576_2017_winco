

class Item:

    def __init__(self, item_no, category, location):
        self.item_no = item_no
        self.category = category
        self.location = location

    def __str__(self):
        return "<Item No: %s Location: %s>" % (self.item_no, self.location)

    def __repr__(self):
        return "<Item No: %s Location: %s>" % (self.item_no, self.location)

    def set_location(self, location):
        self.location = location

    def deepcopy(self):
        item = Item(self.item_no, self.category, self.location)
        return item
