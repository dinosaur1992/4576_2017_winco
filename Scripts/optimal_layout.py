import random
import xlwings as xw
import pandas

from datetime import datetime
from multiprocessing import Lock, Pool, cpu_count
from multiprocessing.managers import BaseManager, NamespaceProxy

import data_processor
from adjacency_analysis import analyze_adjacency
from adjacency_analysis import analyze_frequency
from DistanceAnalyzer import DistanceAnalyzer

# Global variable
result_file = "..\\Results\\current_best_layout.txt"
data_file = "..\\Data\\OrderDetails_Decoded_07-12_2015.xlsx"
new_base_location_sheet_name = "NewBaseLocation"
update_lock = Lock()
print_lock = Lock()


# Helper class
class Node(object):
    def __init__(self, data, frequency, affinity, result_file, item_list=None):
        self.data = data
        self.distance_analyzer = DistanceAnalyzer(data)
        if item_list:
            self.distance_analyzer.set_item_list(item_list)
        self.frequency = frequency
        self.affinity = affinity
        self.result_file = result_file
        self.cost = self.calculate_cost()

    # Use formula to calculate cost
    def calculate_cost(self):
        alpha = 1
        beta = 0
        item_list = self.distance_analyzer.item_list
        location_list = self.distance_analyzer.location_list

        total_frequency_score = 0
        for item in item_list.values():
            count = 0
            if item.item_no in self.frequency.keys():
                count = self.frequency[item.item_no]
            distance = self.distance_analyzer.calculate_distance_between_two_location(location_list[item.location].picking_zone)
            total_frequency_score += count * distance * location_list[item.location].item_count

        total_affinity_score = 0
        '''for pivot_item in item_list.values():
            for item in item_list.values():
                count = 0
                if pivot_item.item_no in self.affinity.keys() and item.item_no in self.affinity[pivot_item.item_no].keys():
                    count = self.affinity[pivot_item.item_no][item.item_no]
                distance = self.distance_analyzer.calculate_distance_between_two_location(location_list[item.location].picking_zone, location_list[pivot_item.location].picking_zone)
                total_affinity_score += count * distance * math.sqrt(location_list[item.location].item_count * location_list[pivot_item.location].item_count)
        '''
        total_cost = alpha * total_frequency_score + beta * total_affinity_score
        return total_cost

    # Generate a random neighbor by swapping location of 2 items.
    def get_random_neighbor(self):
        item_list = self.distance_analyzer.item_list

        ind1 = 0
        ind2 = 0
        while not self.validate_swap(ind1, ind2):
            ind1 = int(random.random() * len(item_list))
            ind2 = int(random.random() * len(item_list))

        new_item_list = {key: value.deepcopy() for key, value in item_list.items()}
        key_list = list(new_item_list.keys())
        location = new_item_list[key_list[ind1]].location
        new_item_list[key_list[ind1]].set_location(new_item_list[key_list[ind2]].location)
        new_item_list[key_list[ind2]].set_location(location)

        return new_item_list

    # Validate a swap. If the difference in item_count of the 2 locations is less than 2, then it is a valid swap.
    def validate_swap(self, ind1, ind2):
        item_list = self.distance_analyzer.item_list
        location_list = self.distance_analyzer.location_list
        if not ind1 == ind2:
            try:
                key_list = list(item_list.keys())
                location1_str = item_list[key_list[ind1]].location
                location2_str = item_list[key_list[ind2]].location
                count1 = location_list[location1_str].item_count
                count2 = location_list[location2_str].item_count
                if abs(count1 - count2) <= 2:
                    return True
            except:
                return False
        return False

    # Evaluate a new layout by comparing current layout with a neighbor.
    # If neighbor's cost is less than current cost,
    # then set current layout to be that of the neighbor, and recalculate current layout's cost.
    def evaluate_new_layout(self):
        neighbor = Node(self.data, self.frequency, self.affinity, self.result_file, self.get_random_neighbor())  # choose random neigh

        if neighbor.cost < self.cost:

            update_lock.acquire()
            new_item_list = neighbor.distance_analyzer.item_list
            self.distance_analyzer.set_item_list(new_item_list)
            self.cost = self.calculate_cost()

            # Save current best to results folder
            with open(result_file, 'w+') as file:
                item_list = self.distance_analyzer.item_list
                for item in item_list.values():
                    file.write(str(item.item_no) + '\t' + item.location + '\n')
            update_lock.release()


def evaluate_new_layout(node_proxy):
    node_proxy.evaluate_new_layout()


# Multiprocessing manager helper class
class NodeManager(BaseManager):
    pass


# Multiprocessing proxy helper class
class NodeProxy(NamespaceProxy):
    _exposed_ = ('__getattribute__', '__setattr__', '__delattr__', 'evaluate_new_layout')

    def evaluate_new_layout(self):
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod(self.evaluate_new_layout.__name__)

NodeManager.register('Node', Node, NodeProxy)


# Calculate frequency for given items based upon given orders.
def get_frequency_data(item_list, order_list):
    frequency = analyze_frequency(item_list.keys(), order_list)
    return frequency


# Calculate affinity data for each pair of given items based upon given orders.
def get_affinity_data(item_list, order_list):
    adjacency_matrix = analyze_adjacency(item_list.keys(), order_list)
    return adjacency_matrix


# Main function to maintain multiprocessing environment and find best layout eventually
def find_best_layout(data=None):
    if not data:
        data = data_processor.main()
    print("Calculating frequency data")
    frequency = get_frequency_data(data["item_list"], data["order_list"])
    print("Calculating affinity data")
    affinity = get_affinity_data(data["item_list"], data["order_list"])

    print("Finding optimal layout...")
    manager = NodeManager()
    manager.start()
    node = manager.Node(data, frequency, affinity, result_file)

    max_ite = len(node.distance_analyzer.item_list) ** 2 / 2
    # max_ite = 100

    repetitions = 0
    start_time = datetime.now()
    pool = Pool(cpu_count())
    while repetitions < max_ite:
        try:
            pool.apply_async(func=evaluate_new_layout, args=(node,))
            repetitions += 1
            print("\rRepetitions: %d/%d finished. " % (repetitions, max_ite), end='')
        except Exception as e:
            print("Error: %s" % e)
    pool.close()
    print("\nWrapping up... ")
    pool.join()
    end_time = datetime.now()
    print("Time elapsed: ", end_time - start_time)
    final_model = node
    return {"model": final_model, "cost": node.cost}


# Export best layout to Excel sheet
def update_date_sheet(item_list):
    print("Saving new locations to Excel sheet. ")
    book = xw.Book(data_file)
    item_location_dict = {}
    for item in item_list.values():
        if item.item_no not in item_location_dict.keys():
            item_location_dict[item.item_no] = item.location
    sheet = book.sheets[new_base_location_sheet_name]
    sheet.clear_contents()

    df = pandas.DataFrame.from_dict(item_location_dict, orient="index")
    sheet.range('A1').value = df
    book.save()
    book.app.quit()


# main function
def main(data=None):
    result = find_best_layout(data)
    update_date_sheet(result["model"].distance_analyzer.item_list)
    return result["model"].distance_analyzer


if __name__ == "__main__":
    main()

