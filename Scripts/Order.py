from OrderItem import OrderItem


class Order:

    def __init__(self, id, order_date, required_date):
        self.id = id
        self.order_date = order_date
        self.required_date = required_date
        self.items = []
        self.item_nos = []
        self.shortest_path = {}

    def add_item(self, item):
        if type(item) is OrderItem:
            if item.item_no not in self.item_nos:
                self.items.append(item)
                self.item_nos.append(item.item_no)
        else:
            raise Exception("Order: Cannot add an item that is not a order_item type. ")

    def set_shortest_path(self, shortest_path):
        self.shortest_path = shortest_path

    def contain_item(self, item_no):
        if item_no in self.item_nos:
            return True
        return False

    def get_item(self, item_no):
        for item in self.items:
            if item.item_no == item_no:
                return item
        print("Error: Item %d not found. " % item_no)
        return None
