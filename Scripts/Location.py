

class Location:

    def __init__(self, location_name, item_count, picking_zone):
        self.location_name = location_name
        self.item_count = item_count
        self.picking_zone = picking_zone

    def __str__(self):
        return "<Location Name:%s Picking_Zone: %s>" % (self.location_name, self.picking_zone)

    def __repr__(self):
        return "<Location Name:%s Picking_Zone: %s>" % (self.location_name, self.picking_zone)


class PickingZone:
    def __init__(self, _id, section, x_coord, y_coord):
        self.id = _id
        self.section = section
        self.x_coord = x_coord
        self.y_coord = y_coord

    def __repr__(self):
        return "<PickingZone Id:%s Section:%s X:%s Y:%s>" % \
               (str(self.id), str(self.section), str(self.x_coord), str(self.y_coord))

    def __str__(self):
        return "<PickingZone Id:%s Section:%s X:%s Y:%s>" % \
               (str(self.id), str(self.section), str(self.x_coord), str(self.y_coord))

    def __eq__(self, other):
        if type(other) is PickingZone:
            if self.id == other.id:
                return True
        return False

    def __hash__(self):
        return hash(self.id)
