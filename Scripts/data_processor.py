import xlrd
import pickle
import os

from datetime import datetime
from functools import lru_cache
from Order import Order
from OrderItem import OrderItem
from OrderList import OrderList
from Item import Item
from Location import PickingZone
from Location import Location

# Constants
data_file = "..\\Data\\OrderDetails_Decoded_07-12_2015.xlsx"
bin_file = "..\\Data\\data_dump"
pwd = "Columbia123"

# sheet_names
order_sheet_name = "Order Information"
product_sheet_name = "Product Information"
location_sheet_name = "Pick Location Item Count"
picking_zone_coord_sheet_name = "Picking Zone Coord"

# order sheet column names
order_id_column_name = "OrderID"
item_no_column_name = "ItemNo"
order_qty_column_name = "OrderQty"
ship_qty_column_name = "ShipQty"
order_date_column_name = "OrderDate"
required_date_column_name = "RequiredDate"

# product sheet column names
# item_no column name is the same as the order sheet
subcategory_column_name = "SubCategory"
location_column_name = "BaseLocation"
new_location_column_name = "NewBaseLocation"

# location sheet column names
# base_location column name is the same as the product sheet
item_count_column_name = "ItemCount"
picking_zone_id_column_name = "PickingZoneID"

# Picking zone coord sheet column names
# picking_zone_id column name is the same as the location sheet
section_column_name = "Section"
x_coord_column_name = "X"
y_coord_column_name = "Y"


# Parse order data
def process_order_data(wb):
    order_list = OrderList()
    order_sheet = wb.sheet_by_name(order_sheet_name)

    # Process Header
    header = process_header(order_sheet, 0)

    order_id_column_index = header.index(order_id_column_name)
    item_no_column_index = header.index(item_no_column_name)
    order_qty_column_index = header.index(order_qty_column_name)
    ship_qty_column_index = header.index(ship_qty_column_name)
    order_date_column_index = header.index(order_date_column_name)
    required_date_column_index = header.index(required_date_column_name)

    # Process Record
    for row in order_sheet.get_rows():
        if try_parse_int(row.__getitem__(order_id_column_index).value):
            order_id = int(row.__getitem__(order_id_column_index).value)
            item_no = int(row.__getitem__(item_no_column_index).value)
            order_qty = int(row.__getitem__(order_qty_column_index).value)
            ship_qty = int(row.__getitem__(ship_qty_column_index).value)
            order_date = datetime(*xlrd.xldate_as_tuple(
                row.__getitem__(order_date_column_index).value, wb.datemode)).date()
            if not row.__getitem__(required_date_column_index).value == '':
                required_date = datetime(*xlrd.xldate_as_tuple(
                    row.__getitem__(required_date_column_index).value, wb.datemode)).date()
            else:
                required_date = ''

            order_item = OrderItem(item_no, order_qty, ship_qty)
            if order_list.contain_order(order_id):
                order = order_list.get_order(order_id)
                if order is not None:
                    order.add_item(order_item)
                else:
                    raise Exception("Order is found as None. ")
            else:
                order = Order(order_id, order_date, required_date)
                order.add_item(order_item)
                order_list.add_order(order)
    return order_list


# Parse Item data
def process_item_data(wb):
    item_list = {}
    new_item_list = {}
    item_sheet = wb.sheet_by_name(product_sheet_name)

    # Process Header
    header = process_header(item_sheet, 0)

    item_no_column_index = header.index(item_no_column_name)
    subcategory_column_index = header.index(subcategory_column_name)
    location_column_index = header.index(location_column_name)
    new_location_column_index = header.index(new_location_column_name)

    for row in item_sheet.get_rows():
        if try_parse_int(row.__getitem__(item_no_column_index).value):
            item_no = int(row.__getitem__(item_no_column_index).value)
            subcategory = str(row.__getitem__(subcategory_column_index).value)
            location = str(row.__getitem__(location_column_index).value).upper()
            new_location = str(row.__getitem__(new_location_column_index).value).upper()

            item = Item(item_no, subcategory, location)
            if item_no not in item_list.keys():
                item_list[item_no] = item

            new_item = Item(item_no, subcategory, new_location)
            if item_no not in new_item_list.keys():
                new_item_list[item_no] = new_item
    return item_list, new_item_list


# Parse location and picking zone data
def process_location_data(wb):
    location_list = {}
    picking_zone_list = {}

    # Process picking zone data
    picking_zone_sheet = wb.sheet_by_name(picking_zone_coord_sheet_name)

    # Process header
    picking_zone_sheet_header = process_header(picking_zone_sheet, 0)

    picking_zone_id_column_index = picking_zone_sheet_header.index(picking_zone_id_column_name)
    section_column_index = picking_zone_sheet_header.index(section_column_name)
    x_coord_column_index = picking_zone_sheet_header.index(x_coord_column_name)
    y_coord_column_index = picking_zone_sheet_header.index(y_coord_column_name)

    for row in picking_zone_sheet.get_rows():
        if try_parse_int(row.__getitem__(picking_zone_id_column_index).value):
            picking_zone_id = int(row.__getitem__(picking_zone_id_column_index).value)
            section = int(row.__getitem__(section_column_index).value)
            x_coord = int(row.__getitem__(x_coord_column_index).value)
            y_coord = int(row.__getitem__(y_coord_column_index).value)
            picking_zone = PickingZone(picking_zone_id, section, x_coord, y_coord)
            if picking_zone_id not in picking_zone_list.keys():
                picking_zone_list[picking_zone_id] = picking_zone

    # Process location data
    location_sheet = wb.sheet_by_name(location_sheet_name)

    # Process header
    location_sheet_header = process_header(location_sheet, 0)

    location_column_index = location_sheet_header.index(location_column_name)
    item_count_column_index = location_sheet_header.index(item_count_column_name)
    location_sheet_picking_zone_id_column_index = location_sheet_header.index(picking_zone_id_column_name)

    for row in location_sheet.get_rows():
        if try_parse_int(row.__getitem__(location_sheet_picking_zone_id_column_index).value):
            location_name = str(row.__getitem__(location_column_index).value)
            item_count = int(row.__getitem__(item_count_column_index).value)
            picking_zone_id = int(row.__getitem__(location_sheet_picking_zone_id_column_index).value)
            picking_zone = picking_zone_list[picking_zone_id]
            location = Location(location_name, item_count, picking_zone)
            if location_name not in location_list.keys():
                location_list[location_name] = location
    return location_list, picking_zone_list


# Open Excel sheet
def open_excel_wb():
    wb = xlrd.open_workbook(data_file)
    return wb


# Helper function to get header of a sheet
def process_header(sheet, header_row_index):
    header = []
    row = sheet.row_values(header_row_index)
    header.extend(row)
    return header


# Helper function to try parse string into int
def try_parse_int(string):
    try:
        int(string)
        return True
    except ValueError:
        return False


# Main function
def main(debug=False):
    if debug and os.path.exists(bin_file):
        print("Debug mode on. Loading static file...")
        data = pickle.load(open(bin_file, "rb"))
        return data

    print('Opening Excel Workbook...')
    wb = open_excel_wb()
    print('Processing order data...')
    order_list = process_order_data(wb)
    print('Processing product data...')
    item_list, new_item_list = process_item_data(wb)
    print('Processing location data...')
    location_list, picking_zone_list = process_location_data(wb)
    print('Data loading finished. ')
    data = {"order_list": order_list, "item_list": item_list, "new_item_list": new_item_list,
            "location_list": location_list, "picking_zone_list": picking_zone_list}
    pickle.dump(data, open(bin_file, "wb"))

    return data


if __name__ == "__main__":
    main()
