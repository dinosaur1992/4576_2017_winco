
import threading
import sa_shortest_path
import data_processor

from datetime import datetime
from Location import PickingZone

source_node = PickingZone("Source", 0, 0, 0)
total_distance = 0.0
total_order = 0
distance_dict = {}
path_dict = {}
update_lock = threading.Lock()


# Helper class to run order analysis in a multi-threaded manner
class ThreadToCalculateDistance(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        threading.Thread.start(self)

    @staticmethod
    def calculate_order_distance(order, item_list, location_list, distance_matrix):
        items = order.items
        item_nos = set([item.item_no for item in items])
        location_names = set([item.location for item in item_list.values() if item.item_no in item_nos])
        locations = [location_list[k] for k in location_list.keys() if k in location_names]
        # Dedup picking zones
        picking_zones = list(set([location.picking_zone for location in locations]))

        # divide into 2 sections
        picking_zones_1_2 = [source_node]
        picking_zones_1_2.extend([p for p in picking_zones if p.section != 3])
        picking_zones_3 = [source_node]
        picking_zones_3.extend([p for p in picking_zones if p.section == 3])
        picking_zones_1_2_dict = {}
        picking_zones_3_dict = {}

        # Use simulated annealing algorithm to calculate shortest path for all orders.
        if len(picking_zones_1_2) >= 0 and len(picking_zones_3) >= 0:
        # Exact algorithm can only handle up to 9 picking zones at a time.
        # if len(picking_zones_1_2) <= 9 and len(picking_zones_3) <= 9:
            order_item_distance_matrix_1_2 = []
            for pivot_zone in picking_zones_1_2:
                picking_zones_1_2_dict[len(order_item_distance_matrix_1_2)] = pivot_zone.id
                order_item_distance_matrix_1_2.append([])
                for zone in picking_zones_1_2:
                    distance = distance_matrix[pivot_zone.id][zone.id]
                    order_item_distance_matrix_1_2[-1].append(distance)

            order_item_distance_matrix_3 = []
            for pivot_zone in picking_zones_3:
                picking_zones_3_dict[len(order_item_distance_matrix_3)] = pivot_zone.id
                order_item_distance_matrix_3.append([])
                for zone in picking_zones_3:
                    distance = distance_matrix[pivot_zone.id][zone.id]
                    order_item_distance_matrix_3[-1].append(distance)

            # Exact algorithm
            # shortest_path_1_2 = calculate_shortest_path(order_item_distance_matrix_1_2)
            # shortest_path_3 = calculate_shortest_path(order_item_distance_matrix_3)

            # Simulated Annealing
            shortest_path_1_2 = sa_shortest_path.sa(order_item_distance_matrix_1_2)
            shortest_path_3 = sa_shortest_path.sa(order_item_distance_matrix_3)

            # Piece together results from 2 separate parts
            picking_zone_path = [picking_zones_1_2_dict[i] for i in shortest_path_1_2["visited"]]
            picking_zone_path.extend([picking_zones_3_dict[i] for i in shortest_path_3["visited"]])
            item_path = []
            for zone_id in picking_zone_path:
                item_path.extend([order_item.item_no for order_item in items
                                  if location_list[item_list[order_item.item_no].location].picking_zone.id == zone_id])

            shortest_path = {"distance": shortest_path_1_2["distance"] + shortest_path_3["distance"],
                             "path": item_path}
            # print(shortest_path)

            # Update total statistics
            update_lock.acquire()
            global total_distance
            total_distance += shortest_path["distance"]
            global total_order
            total_order += 1
            global distance_dict
            if order.id not in distance_dict.keys():
                distance_dict[order.id] = shortest_path["distance"]
            global path_dict
            if order.id not in path_dict.keys():
                path_dict[order.id] = item_path
            update_lock.release()


# DistanceAnalyzer class
class DistanceAnalyzer:

    def __init__(self, data=None):
        if not data:
            data = data_processor.main()
        self.order_list = data["order_list"]
        self.item_list = data["item_list"]
        self.new_item_list = data['new_item_list']
        self.location_list = data["location_list"]
        picking_zone_list = data["picking_zone_list"]
        self.distance_matrix = self.construct_distance_matrix(picking_zone_list)

    # Calculate avg distance travelled per order
    def calculate_avg_distance(self):
        global total_distance
        total_distance = 0.0
        global total_order
        total_order = 0
        global distance_dict
        distance_dict = {}
        start = datetime.now()
        average_distance, distance_dict, path_dict = self.analyze_distance()
        end = datetime.now()
        print("Average distance: ", average_distance)
        print("Time elapsed: ", end - start)
        return average_distance, distance_dict

    # Setter of item_list
    def set_item_list(self, item_list):
        self.item_list = item_list

    # Main function that loops through all orders and find shortest path for each one.
    def analyze_distance(self, orders=None, use_new_loc=False):
        global total_order
        global total_distance
        global distance_dict
        global path_dict

        total_order = 0
        total_distance = 0.0
        distance_dict = {}
        path_dict = {}

        order_list = self.order_list.order_list.values()
        item_list = self.item_list
        if orders:
            order_list = [order for order in order_list if order.id in orders]
        if use_new_loc:
            item_list = self.new_item_list
        location_list = self.location_list
        distance_matrix = self.distance_matrix
        threads = []
        counter = 0
        for order in order_list:
            print("\rChecking order %d. %d%% finished. " % (order.id, counter / len(order_list) * 100),
                  end='')
            counter += 1

            # Create a separate thread for each order
            try:
                thread = ThreadToCalculateDistance()
                thread.calculate_order_distance(order, item_list, location_list, distance_matrix)
                threads.append(thread)
            except Exception as e:
                print("Error: %s" % e)
        print("\rChecking order %d, 100%% finished. " % list(order_list)[-1].id)

        # Wait until all threads finish
        for t in threads:
            t.join()

        print("Total # of orders analyzed: ", total_order)
        average_distance = total_distance / total_order
        return average_distance, distance_dict, path_dict

    # Construct a distance matrix between any two picking zones
    def construct_distance_matrix(self, picking_zones):
        picking_zones["Source"] = source_node

        matrix = {}
        for pivot_zone in picking_zones.values():
            pivot_zone_id = pivot_zone.id
            matrix[pivot_zone_id] = {}
            for zone in picking_zones.values():
                distance = self.calculate_distance_between_two_location(pivot_zone, zone)
                matrix[pivot_zone_id][zone.id] = distance

        return matrix

    # Calculate distance between 2 locations
    def calculate_distance_between_two_location(self, location1, location2=source_node):
        if type(location1) is not PickingZone or type(location2) is not PickingZone:
            raise TypeError

        section1 = location1.section
        x1 = location1.x_coord
        y1 = location1.y_coord
        zone1 = location1.id

        section2 = location2.section
        x2 = location2.x_coord
        y2 = location2.y_coord

        # Section = 0 means source node. From source to any point is a distance of its x coord.
        if section1 == 0 and section2 != 0:
            distance = x2
            return distance
        if section1 != 0 and section2 == 0:
            distance = x1
            return distance

        # To go from section 1&2 to section 3, one has to return to origin first.
        if (section1 == 3 and section2 != 3) or (section1 != 3 and section2 == 3):
            distance = self.calculate_distance_between_two_location(location1) + \
                       self.calculate_distance_between_two_location(location2)
            return distance

        # Distance within the same zone is 0
        if x1 == x2 and y1 == y2:
            distance = 0
            return distance

        # General Case:  Within the same large area, calculate distance between 2 points
        if x1 == x2:
            distance = abs(y1 - y2) + x1 / (zone1 % 10)
        else:
            distance = abs(y1 - y2) + abs(x1 - x2)
        return distance


# Exact algorithm to calculate shortest path that goes through all nodes.
def calculate_shortest_path(matrix):
    length = len(matrix)
    all_nodes = range(length)
    solutions = [{"visited": [0], "distance": 0}]

    for count in range(length):
        new_solutions = []
        reset_solution = True
        for sol in solutions:
            visited = sol["visited"]
            distance = sol["distance"]
            if len(set(all_nodes)) != len(set(visited)):
                for i in set(all_nodes) - set(visited):
                    new_visited = visited[:]
                    new_visited.append(i)
                    dist_travelled = matrix[i][visited[-1]]
                    new_distance = distance + dist_travelled
                    new_solutions.append({"visited": new_visited, "distance": new_distance})
            else:
                reset_solution = False
                come_back_dist = matrix[0][visited[-1]]
                distance += come_back_dist
                sol["distance"] = distance
        if reset_solution:
            solutions = new_solutions
    sorted_sol = sorted(solutions, key=lambda e: e["distance"])
    shortest_path = sorted_sol[0]
    if shortest_path["distance"] != 0:
        shortest_path["visited"].append(0)
    return shortest_path


# Local test function
def test():
    test_matrix = [[0, 1, 2, 3, 4, 5, 6, 1, 8, 7],
                   [1, 0, 1, 2, 6, 4, 5, 2, 6, 1],
                   [2, 1, 0, 3, 4, 1, 2, 3, 8, 3],
                   [3, 2, 3, 0, 3, 6, 9, 8, 2, 7],
                   [4, 6, 4, 3, 0, 5, 2, 8, 7, 5],
                   [5, 4, 1, 6, 5, 0, 2, 2, 7, 3],
                   [6, 5, 2, 9, 2, 2, 0, 7, 3, 9],
                   [1, 2, 3, 8, 8, 2, 7, 0, 4, 2],
                   [8, 6, 8, 2, 7, 7, 3, 4, 0, 1],
                   [7, 1, 3, 7, 5, 3, 9, 2, 1, 0]]
    import datetime
    start = datetime.datetime.now()
    print(calculate_shortest_path(test_matrix))
    end = datetime.datetime.now()
    print(end - start)


if __name__ == "__main__":
    da = DistanceAnalyzer()
    da.calculate_avg_distance()
