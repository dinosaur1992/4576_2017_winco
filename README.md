
This repository contains codes and deliverables for the Winco Project of course 4576 - Inductry Project (Spring 2017)

Date Created: Feb 1st, 2017

# Summary #

This is a python repository to calculate the best layout for the given inventory settings of Winco US. 
We use transaction data for over 3000+ catalog items and ~60000 orders, and try to find the optimal layout so as to minimize the average distance travelled when the pickers collect items for one order. 

# Repository Setup  

The following is required to run our program. 

## Python Setup 
* Python 3.x is required. 
* The following python modules is required. 
	* xlrd
	* xlwings	
	* pandas
	* flask

## Data Setup 
* All data files should be under __Data__ folder 
* Workbook __OrderDetails_Decoded_07-12_2015.xlsx__ - main data source
	* Sheet - __Order Information__, with following fields
		* OrderID
		* ItemNo
		* OrderQty
		* ShipQty
		* OrderDate
		* RequiredDate
	* Sheet - __Product Informaiton__, with following fields
		* ItemNo
		* SubCategory
		* BaseLocation
		* NewBaseLocation
	* Sheet - __Picking Zone Item Count__, with following fields
		* BaseLocation
		* ItemCount
		* PickingZoneID
	* Sheet - __Picking Zone Coord__, with following fields
		* PickingZoneID
		* Section
		* X
		* Y
	* Sheet - __NewBaseLocation__
		* Can be left as empty 
* Workbook __Heat_Map_Generator.xlsm__ - Heat map generator
	* Need to have macro enabled 
	* Sheet - __original__, layout of the warehouse with location names
	* Sheet - __rack_frequency__, # of orders that contain items which are located in the given rack 
	* Sheet - __layout__, a sheet with conditional formatting that displays the rack order frequency (from rack_frequency sheet) as a heat map

# Steps to Run the Program 

## Main Script 
To run main script, execute command `python main.py` from __Scripts__ folder. 
The script will evaluate the current layout, and then find optimal layout and eventually evaluate new layout. 

_Warning: This program might run for a long time and consume a lot of memory._

## Web Application  
The web app is built using Flask. To start the app server, execute command `python app.py` from __WebApp__ folder. 
The app will listen on port 5000, and is accessible from this [link](http://127.0.0.1:5000/) after server is up and ready. 

# Scripts Documentation  

The repository contains the following scripts. 

## Scripts folder 

### OrderList. py, Order.py, OrderItem.py, Item.py, Location.py ###
Class definitions. These scripts are not executable. For detailed data structure, please reference [Class Diagram]()

### data_processor.py 
This executable script loads data from Excel and parse them into the object classes defined above. It will also save the parsed data as a binary locally for further debug usage.  

### DistanceAnalyzer.py  
This executable script evaluates a layout and calculate average shortest distance travelled per order. 

### sa_shortest_path.py  
This is a helper script used by DistanceAnalyzer to calculate the shortest path to pick up all items within a given order. It implemented Simulated Annealing algorithm. This script is not executable on its own. 

### optimal_layout.py  
This executable script finds the optimal layout by swapping 2 items at a time and evaluating the new layout using total frequency score and total affinity score. 

### rack_frequency_analysis 
This executable script loads the new layout, and then generates a order count for each rack location and eventually calls Excel VBA to generate an updated heat map. 

### adjacency_analysis.py 
This executable script generates item affinity matrix, defined as count of orders which contain each pair of items. 

### main.py  
This executable script is the main script of this repository. It evaluates the current layout, and then finds optimal layout and eventually evaluates new layout. 

## WebApp folder
### app.py 
Main program to start the web application 


